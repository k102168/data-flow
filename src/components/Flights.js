import React from 'react';
import Passanger from './Passanger';


class Flights extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			hide:false
		}

		this.handleClick = this.handleClick.bind(this);
	}
	handleClick(){
			this.setState({
				hide: !this.state.hide
			})
		}

	
	render(){
		const flight = this.props.flight;
		//const passengerCount = flight.passengers.length
		//const passenger = flight.passengers.map((p,i)=><Passanger key={i} passenger={p} passengerCount={passengerCount}/>);
		return(
			<ul>
				<li><button onClick={this.handleClick} >{flight.flight}</button></li>
				{
					this.state.hide?

				<ul>
					<li>{flight.arrival}</li>
					<li>{flight.duration}</li>
					<li>{flight.departure}</li>
					<Passanger key={flight.id} passengers={flight.passengers}/>
				</ul>:null
				}
				
				{/*<ul><Passanger key={flight.flight}/></ul>*/}
			</ul>
		);
	}
}

export default Flights
