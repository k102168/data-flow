import React from 'react';
import Passports from './Passports';


class  Passangers extends React.Component {
	constructor(props) {
		super(props);
		this.state ={
			phide:false,
			pdHide:false
		}
		this.handleClickPassanger = this.handleClickPassanger.bind(this);
		this.handleClickPassangerDetail = this.handleClickPassangerDetail.bind(this);
	}
	handleClickPassanger(){
		this.setState({
			phide:!this.state.phide
		})
	}
	handleClickPassangerDetail(){
		this.setState({
			pdHide:!this.state.pdHide
		})
	}
	render(){
		const passenger = this.props.passengers.map((p)=>(
			<ul>
				<li><button onClick={this.handleClickPassangerDetail}>{p.name}</button></li>
				{this.state.pdHide?
					<ul>
						<li>{p.age}</li>
						<li>{p.nationality}</li>
						<Passports passports={p.passport}/>
					</ul>:null
				}
				
			</ul>
		));
		return (
			<div>
				<li>
				<button onClick={this.handleClickPassanger}>Passengers :({this.props.passengers.length})</button>
				</li>
				{this.state.phide?passenger:null}
			</div>
		);
	}
}


export default Passangers;
