import React from 'react';

class Passports extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			hide:false
		}
		this.handleClick = this.handleClick.bind(this)
	}
	handleClick(){
		this.setState({
			hide:!this.state.hide
		})
	}

	render(){

		const passport = this.props.passports.map((ps)=>(
					<div>
					{ this.state.hide?
						<ul >
							<li>{ps.type}</li>
							
								<ul>
										<li>{ps.expiryDate}</li>
								</ul>
							
						</ul>:null
					}
					</div>
				));

		return (
				<div>
					<li><button onClick={this.handleClick}>Passport:</button></li>
					{passport}
				</div>
			);
	}
}


export default Passports